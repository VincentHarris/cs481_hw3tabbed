﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace CS481_HW3Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class mario : ContentPage
    {
        public mario()
        {
            InitializeComponent();
        }


        protected override void OnAppearing()
        {
            DisplayAlert("Wahoo!", "Super Mario World is now on sale!", "Yipee!");
        }

        protected override void OnDisappearing()
        {
            DisplayAlert("Thank you so much-a!", "Good luck, Plumber!", "Okey Dokey!");
        }
        //Referenced from https://docs.microsoft.com/en-us/xamarin/essentials/open-browser?tabs=android 
        //Browser.OpenAsync uses the url attached to go that website and the
        //SystemPreferred will open it using the devices preferred app for whatever link is applies
        private void marioClick(object sender, EventArgs e)
        {
            Browser.OpenAsync("https://www.youtube.com/watch?v=EQ3clCcwHFc", BrowserLaunchMode.SystemPreferred);
        }
    }
}