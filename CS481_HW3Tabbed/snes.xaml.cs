﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace CS481_HW3Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class snes : ContentPage
    {
        public snes()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            DisplayAlert("Now you're playing with power!", "SNES is now on sale!", "OK");
        }

        protected override void OnDisappearing()
        {
            DisplayAlert("Super power, for super players!", "Thank you so much for choosing the SNES!", "RADICAL!!");
        }
        //Referenced from https://docs.microsoft.com/en-us/xamarin/essentials/open-browser?tabs=android 
        //Browser.OpenAsync uses the url attached to go that website and the
        //SystemPreferred will open it using the devices preferred app for whatever link is applies
        private void snesClick(object sender, EventArgs e)
        {
            Browser.OpenAsync("https://www.youtube.com/watch?v=e-SjFG6wrhs", BrowserLaunchMode.SystemPreferred);
        }
    }
}