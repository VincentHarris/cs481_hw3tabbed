﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace CS481_HW3Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class metroid : ContentPage
    {
        public metroid()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            DisplayAlert("INCOMING TRANSMISSION", "Super Metroid is now on sale!", "Confirm Mission");
        }

        protected override void OnDisappearing()
        {
            DisplayAlert("INCOMING TRANSMISSION", "Mission Debrief has concluded", "Acknowledged");
        }
        //Referenced from https://docs.microsoft.com/en-us/xamarin/essentials/open-browser?tabs=android 
        //Browser.OpenAsync uses the url attached to go that website and the
        //SystemPreferred will open it using the devices preferred app for whatever link is applies
        private void metroidClick (object sender, EventArgs e)
        {
            Browser.OpenAsync("https://www.youtube.com/watch?v=aG6c2JOuGuk", BrowserLaunchMode.SystemPreferred);
        }
    }
}