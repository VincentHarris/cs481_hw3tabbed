﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace CS481_HW3Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class zelda : ContentPage
    {
        public zelda()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
            {
              DisplayAlert("HIYUHH", "Legend of Zelda: Link to the Past is now on sale!", "HAYYYAHHH");
            }

        protected override void OnDisappearing()
        {
            DisplayAlert("HEEEETUT", "Now go save Princess Zelda!!", "UOOHHHHHH");
        }
        //Referenced from https://docs.microsoft.com/en-us/xamarin/essentials/open-browser?tabs=android 
        //Browser.OpenAsync uses the url attached to go that website and the
        //SystemPreferred will open it using the devices preferred app for whatever link is applies
        private void zeldaClick(object sender, EventArgs e)
        {
            Browser.OpenAsync("https://www.youtube.com/watch?v=cjpHzLYHkwA", BrowserLaunchMode.SystemPreferred);
        }

    }
}